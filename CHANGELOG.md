# Changelog

Uses [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [0.3.1] - 2025-01-13
### Added
- ansible role `geerlingguy.docker`

## [0.3.0] - 2024-12-01
### Updated
- base OS to Debian 12.8
- ansible
- ansible-lint

## [0.2.0] - 2024-09-18
### Added
- packages `sshpass`

## [0.1.1] - 2023-07-18
### Added
- packages `nc, curl`
- python packages `dnspropagation`
- ansible collection `community.docker`


## [0.1] - 2023-05-25
### Init
