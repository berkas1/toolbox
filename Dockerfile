FROM debian:12.8
LABEL version="0.3.1"

ENV ANSIBLE_VERSION=2.17.6
ENV ANSIBLELINT_VERSION=24.9.2

RUN apt-get -y update
RUN apt-get install -y python3-pip pipx iputils-ping htop mtr git nano vim bsdmainutils jq wget gpg lsb-release bash-completion python3-openstackclient mc curl sshpass
RUN openstack complete | tee /etc/bash_completion.d/osc.bash_completion > /dev/null

RUN adduser -q --shell /bin/bash user
USER user
ENV PATH="${PATH}:/home/user/.local/bin"

#RUN python3 -m pip install --upgrade pip
RUN python3 -m pipx install yq
RUN python3 -m pipx install ansible-core==${ANSIBLE_VERSION}
RUN python3 -m pipx install ansible-lint==${ANSIBLELINT_VERSION} 
RUN pipx install dnspropagation
RUN ansible-galaxy collection install ansible.posix community.general devsec.hardening community.docker
RUN ansible-galaxy role install geerlingguy.docker

#User_specific
RUN mkdir /home/user/persistent

#User_specific
WORKDIR /home/user/repo
